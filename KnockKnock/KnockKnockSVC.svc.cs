﻿using System;
using System.ServiceModel;
using TriangleType = KnockKnock.Enums.TriangleType;

namespace KnockKnock
{
    [ServiceBehavior(Name = "RedPill", Namespace = "http://KnockKnock.readify.net")]
    public class KnockKnockSVC : IKnockKnock
    {
        //Applicant Token
        private static readonly Guid ApplicantToken = new Guid("c5f86723-249f-4458-b4fc-23811ef11bb1");

        //Fibonacci
        private const int MaxFibValue = 92;
        private const int MinFibValue = -92;

        public Guid WhatIsYourToken()
        {
             return ApplicantToken;
        }

        public long FibonacciNumber(long val)
        {
            if (val < MinFibValue || val > MaxFibValue)
                throw new ArgumentOutOfRangeException("val", "Entered value is out of range");
            
            if (val == 0) 
                return 0;

            long a = 0;
            long b = 1;
            var index = Math.Abs(val);

            for (int i = 0; i < index; i++)
            {
                long temp = a;
                a = b;
                b += temp;
            }

            if (val < 0 && index%2 == 0)
                return -a;
            
            return a;
        }

        public TriangleType WhatShapeIsThis(int side1, int side2, int side3)
        {
            if (!Helpers.ShapesHelpers.ValidateTriangle(side1, side2, side3))
                return TriangleType.Error;
            
            if (side1 == side2 && side2 == side3 && side3 == side1)
                return TriangleType.Equilateral;

            if (side1 == side2 || side2 == side3 || side3 == side1)
                return TriangleType.Isosceles;

            return TriangleType.Scalene;
        }

        public string ReverseWords(string inputString)
        {
            if (inputString == null)
                throw new ArgumentNullException("inputString", "inputString cannot be null");

            var input = inputString;
            // Holds the output string chars
            var output = new char[input.Length];
            // Number of charecters written in the output array
            int written = 0;
            int startIndex = 0;
            for (int i = 0; i < input.Length; i++)
            {
                // Current charecter
                char c = input[i];

                if (char.IsWhiteSpace(c) || i == input.Length - 1)
                {
                    int endIndex = char.IsWhiteSpace(c) ? i - 1 : i;
                    //Reached end of the word or end of the string. Add the word chars to the output in a reversed order
                    for (int j = endIndex; j >= startIndex; j--)
                        output[written++] = input[j];

                    startIndex = i + 1;

                    //Add white space in the same position
                    if (char.IsWhiteSpace(c))
                        output[written++] = c;
                }
            }
            return new string(output);
        }
    }
}
