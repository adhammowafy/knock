﻿
namespace KnockKnock.Helpers
{
    public static class ShapesHelpers
    {
        public static bool ValidateTriangle(int side1, int side2, int side3)
        {
            // These sides can form a valid triangle.
            if (side1 <= 0 || side2 <= 0 || side3 <= 0)
                return false;

            if (side1 < side2 + (long)side3 && side2 < side1 + (long)side3 && side3 < side1 + (long)side2)
                return true;
            
            return false;
        }
    }
}