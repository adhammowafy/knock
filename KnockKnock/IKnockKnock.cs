﻿using KnockKnock.Enums;
using System;
using System.ServiceModel;

namespace KnockKnock
{
    [ServiceContract(Name = "IRedPill", Namespace = "http://KnockKnock.readify.net")]
    public interface IKnockKnock
    {
        [OperationContract]
        Guid WhatIsYourToken();

        [OperationContract]
        long FibonacciNumber(long n);

        [OperationContract]
        TriangleType WhatShapeIsThis(int a, int b, int c);

        [OperationContract]
        string ReverseWords(string s);
    }
}
