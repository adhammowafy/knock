﻿namespace KnockKnock.Enums
{
    public enum TriangleType
    {
        Error = 0,
        Equilateral = 1,
        Isosceles = 2,
        Scalene = 3,
    }
}